const user = require('../models/user.js');

// Create and Save a new user
exports.create = (req, res) => {
// Validate request
if(!req.body.content) {
    return res.status(400).send({
        message: "user content can not be empty"
    });
}

// Create a user
const user = new user({
    id: req.body.id,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    age: req.body.age,
    gender: req.body.gender,
    tandcs: req.body.tandcs,
    email: req.body.email
});

// Save user in the database
user.save()
.then(data => {
    res.send(data);
}).catch(err => {
    res.status(500).send({
        message: err.message || "Some error occurred while creating the user."
    });
});
};

// Retrieve and return all users from the database.
exports.findAll = (req, res) => {
    user.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });
};

// Find a single user with a userId
exports.findOne = (req, res) => {
    user.findById(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });            
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params.id
        });
    });
};

// Update a user identified by the Id in the request
exports.update = (req, res) => {
// Validate Request
if(!req.body.content) {
    return res.status(400).send({
        message: "user content can not be empty"
    });
}

// Find user and update it with the request body
user.findByIdAndUpdate(req.params.id, {
    id: req.body.id,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    age: req.body.age,
    gender: req.body.gender,
    tandcs: req.body.tandcs,
    email: req.body.email
}, {new: true})
.then(user => {
    if(!user) {
        return res.status(404).send({
            message: "user not found with id " + req.params.id
        });
    }
    res.send(user);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "user not found with id " + req.params.id
        });                
    }
    return res.status(500).send({
        message: "Error updating user with id " + req.params.id
    });
});
};

// Delete a user with the specified id in the request
exports.delete = (req, res) => {
    user.findByIdAndRemove(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        res.send({message: "user deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete user with id " + req.params.id
        });
    });
};
