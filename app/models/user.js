const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    id: Number,
    firstName: String,
    lastName: String,
    age: Number,
    gender: String,
    tandcs: Boolean,
    email: String
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);
