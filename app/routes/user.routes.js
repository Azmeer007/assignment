module.exports = (app) => {
    const user = require('../controller/user.controller.js');

    // Create a new user
    app.post('/user', user.create);

    // Retrieve all user
    app.get('/user', user.findAll);

    // Retrieve a single user with id
    app.get('/user/:id', user.findOne);

    // Update a user with id
    app.put('/user/:id', user.update);

    // Delete a user with id
    app.delete('/user/:id', user.delete);
}